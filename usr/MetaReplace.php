<?php

// 注册函数名须匹配 DataBaseHD_DIY_文件名_Register
function DataBaseHD_DIY_MetaReplace_Register(&$fnList)
{
  $fnList[] = array("fn" => "DataBaseHD_DIY_MetaReplace_Post", "mod" => "Post", "name" => "替换指定 Meta 字段");
  // 可注册多条，虽然 Post 之外的表可能还没封装 /doge
  // $fnList[] = array("fn" => "DataBaseHD_DIY_MetaReplace_Tag", "mod" => "Tag", "name" => "处理 Tag 表");
}

// 建议函数名 DataBaseHD_DIY_文件名_功能标识
// 与上边 array("fn" => "XXX", ……) 中的 XXX 匹配即可
function DataBaseHD_DIY_MetaReplace_Post(&$post, $csrfToken = "")
{

  // 要修改的 Meta 字段
  $metaKey = "DemoImg";
  $search = "8081";
  $replace = "8082";

  // // 一个 ID 数组
  // $arrID = array(1, 2, 3, 4, 5);
  // // 不属于指定 ID 的跳过
  // if (in_array($post->ID, $arrID) === false) {
  //   return;
  // }


  $tpl = "<p style='color:-color-;'>-id- 丨 -url- 丨 -oldHash- 丨 -newHash- | -Tip- | 「-edit-」「-del-」</p>\n";

  $arrData = array();
  $arrData["-color-"] = "-black-";
  $arrData["-id-"] = $post->ID;
  $arrData["-url-"] = DataBaseHD_DIY_a($post->Url, $post->Title);
  $arrData["-oldHash-"] = crc32($post->Metas->$metaKey);

  // 编辑或删除按钮
  $arrData["-edit-"] = "<a class=\"style-visited\" title=\"{$post->Title}\" target=\"_blank\" href='../../../zb_system/admin/edit.php?act=ArticleEdt&id={$post->ID}'>编辑</a>";
  $arrData["-del-"] = "<a class=\"style-visited\" title=\"{$post->Title}\" target=\"_blank\" onclick=\"return window.confirm('即将删除「{$post->Title}」，请确认！');\" href=\"../../../zb_system/cmd.php?act=ArticleDel&id={$post->ID}&csrfToken={$csrfToken}\">删除</a>";

  // 判断是否存在指定 meta
  if ($post->Metas->HasKey($metaKey) === false) {
    $arrData["-Tip-"] = "<b>不存在</b>";
    echo strtr($tpl, $arrData);
    return;
  }

  // 替换内容
  $post->Metas->$metaKey = str_replace($search, $replace, $post->Metas->$metaKey);


  $arrData["-newHash-"] = crc32($post->Metas->$metaKey);
  $arrData["-Tip-"] = "<b>文章未修改</b>";

  if ($arrData["-oldHash-"] !== $arrData["-newHash-"]) {
    $arrData["-color-"] = "red";
    $bolRlt = $post->Save();
    $arrData["-Tip-"] = $bolRlt ? "<b>保存成功</b>" : "<b>保存失败</b>";
  }


  echo strtr($tpl, $arrData);
}
